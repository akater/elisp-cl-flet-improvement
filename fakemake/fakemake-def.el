;;;  -*- lexical-binding: t -*-

(defconst-with-prefix fakemake
  feature 'cl-flet-improvement
  authors "Dima Akater"
  first-publication-year-as-string "2021"
  org-files-in-order '("cl-flet-improvement")
  site-lisp-config-prefix "10"
  license "GPL-3")
